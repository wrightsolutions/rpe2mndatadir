# Enhance the remote setup so that enhanced monitoring...
# ...relevant to MNs is supported.
# Insert a super server (?netd) in front of bare rpe port for
# ...enhanced access control
# name: git@bitbucket.org:owner/rpe2mndatadir.git
# Run recipe on the remote in standalone mode: /usr/bin/salt-call --local state.sls rpe2mndatadir
# {% set chroot_parent = salt['pillar.get']('chroot_parent','/var/chroot') %}
{% set repo_mn_dir = '/opt/local/repos_mn' %}
{% set repos_group = 'staff' %}
{% set lib_plugins_dir = '/usr/lib/nagios/plugins/' %}


nagios_nrpe:
  # Install Nagios nrpe remote client
  pkg.installed:
    - pkgs:
      - nagios-nrpe-server
#     - monitoring-plugins
      - monitoring-plugins-basic
      - nagios-plugins-common
#     - nagios-plugins-basic
      - nagios-plugins-standard
#     - nagios-plugins
    - install_recommends: false


internet_superserver_plus:
  # systemctl restart rinetd; tailf /var/log/rinetd.log; find /root/ -name 'rinet2allows.py' -ls
  pkg.installed:
    - pkgs:
      - rinetd
    # - install_recommends: false


source_control_plus:
  pkg.installed:
    - pkgs:
      - git
      - less
      - patch
      - rsync
    - install_recommends: false


plugin_python2:
  pkg.installed:
    - pkgs:
      - python-minimal
    - install_recommends: false


sudoers_monitoring_local2:
  file.managed:
  - name: /etc/sudoers.d/92-monitoring_local2
  - contents: |
      # Local Example: nagios    ALL=(ALL) NOPASSWD: /usr/bin/python2 /usr/lib/nagios/plugins/ncon_filter2.py --warn*
      # This file is usually supplementary to an existing /etc/sudoers.d/90-monitoring_local
      # On Debian the Nagios plugin scripts live in /usr/lib/nagios/plugins
      %nagios   ALL=(ALL) NOPASSWD: /bin/fgrep maxconnections test128
      %nagios   ALL=(ALL) NOPASSWD: /bin/fgrep maxconnections /home/*
      %nagios   ALL=(ALL) NOPASSWD: /usr/bin/python2 /usr/lib/nagios/plugins/ncon_filter2.py --warn*
      %nagios   ALL=(ALL) NOPASSWD: /usr/bin/python2 /usr/lib/nagios/plugins/ncon_from_conf_warnonmax1.py stdin
      ALL       ALL=(ALL) NOPASSWD: /usr/bin/uptime, /usr/bin/downtimes
  - order: last
  - backup: minion


repos_directory_mn:
  file.directory:
  - name: {{ repo_mn_dir }}
  - user: root
  # - group: {{ repos_group }}
  - mode: 0755
  - clean: True
  - exclude_pat: '*.sh*'


repo_mn_salt2struct:
  git.latest:
  - name: https://wrightsolutions@bitbucket.org/wrightsolutions/salt2struct.git
  - target: {{ repo_mn_dir }}/salt2struct
  - depth: 1
  - require:
    - pkg: source_control_plus
    - file: repos_directory_mn


repo_mn_procnet2:
  git.latest:
  - name: https://wrightsolutions@bitbucket.org/wrightsolutions/procnet2.git
  - target: {{ repo_mn_dir }}/procnet2
  - depth: 1
  - require:
    - pkg: source_control_plus
    - file: repos_directory_mn


repo_mn_rpe2d:
  git.latest:
  - name: https://wrightsolutions@bitbucket.org/wrightsolutions/rpe2d.git
  - target: {{ repo_mn_dir }}/rpe2d
  - depth: 1
  - require:
    - pkg: source_control_plus
    - file: repos_directory_mn


repo_mn_qmn1ub16:
  # In future this git.latest will be specialised or renamed but historical name for now
  git.latest:
  - name: https://wrightsolutions@bitbucket.org/wrightsolutions/qmn1ub16.git
  - target: {{ repo_mn_dir }}/qmn1ub16
  - depth: 1
  - require:
    - pkg: source_control_plus
    - file: repos_directory_mn


lib_plugins_procnet2_py:
  # Put py plugins into lib plugins directory
  # Includes the .d cfg file 27server_address_localhost.cfg
  cmd.run:
    - name: >
        cp --preserve=timestamps
        --target-directory={{ lib_plugins_dir }}/
        ./*.py
    - cwd: {{ repo_mn_dir }}/procnet2
    - timeout: 60
    - require:
      - pkg: plugin_python2
      - file: repos_directory_mn
      - git: repo_mn_procnet2


