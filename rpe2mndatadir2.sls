# Enhance the remote setup so that enhanced monitoring...
# ...relevant to MNs is supported.
# Insert a super server (?netd) in front of bare rpe port for
# ...enhanced access control
# name: git@bitbucket.org:owner/rpe2mndatadir.git
# Run recipe on the remote in standalone mode: /usr/bin/salt-call --local state.sls rpe2mndatadir2
{% set repo_mn_dir = '/opt/local/repos_mn' %}
# Start of variables for section internet_superserver allowed
{% set inet_super_listen_ip = '54.38.ccc.node' %}
{% set inet_super_listen_port = '56200' %}
{% set inet_super_allowed_ip = '92.bbb.ccc.icinga' %}
# End of variables for section internet_superserver allowed


rpe2d_cfg_local:
  # Force nrpe to listen localhost only and other cfg changes
  # Includes the .d cfg file 27server_address_localhost.cfg
  cmd.run:
    - name: >
        cp --preserve=timestamps
        --target-directory=/etc/nagios/nrpe.d/ 
        ./*.cfg
    - cwd: {{ repo_mn_dir }}/rpe2d/rpe.d
    - timeout: 60


rpe_server_service:
  service:
    - name: nagios-nrpe-server
    - running
    - enable: True
    - watch:
      - cmd: rpe2d_cfg_local
      #- file: /etc/rinetd/conf.d/*


internet_superserver_allowed4:
  # Generate conf file for Rinetd based on port choices given on command line
  # Use Python script that generates an appropriate allow/deny section for this
  # Return code of 151 probably means you have failed to add proper values
  # for the inet_super variables at the top of this sls file
  cmd.run:
    - name: >
        python2 ./rinet2allows.py
        {{ inet_super_listen_ip }} {{ inet_super_listen_port }}
        127.0.0.1 5666
        {{ inet_super_allowed_ip }} > 
        /etc/rinetd.conf
    - cwd: {{ repo_mn_dir }}/qmn1ub16/nrpe/helper
    - timeout: 60


#rpe_allowed4:
  # Generated /etc/nagios/nrpe.d/10allowed_hosts4.cfg from script
  # Use where nrpe is accepting connections directly
  # where .d cfg file 27server_address_localhost.cfg present this section not required
  #cmd.run:
  #  - name: >
  #      python ./10allowed_hosts4.py 92.222.ccc.237 >
  #      /etc/nagios/nrpe.d/10allowed_hosts4.cfg


